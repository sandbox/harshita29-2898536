<?php

namespace Drupal\image_menu;

/**
 * Helper Class for database interaction.
 */
class StorageHelper {

  public $db;

  /**
   * Function to create object of storage helper class.
   */
  public static function instance() {
    static $inst = NULL;
    if ($inst === NULL) {
      $inst = new StorageHelper();
    }
    return $inst;
  }

  /**
   * Constructor of storage helper class.
   */
  public function __construct() {
    $this->db = \Drupal::database();
  }

  /**
   * Function to getData.
   */
  public function getData($plugin_id) {
    $data = $this->db->select('image_menu', 'im')
      ->fields('im')
      ->condition('plugin_id', $plugin_id, '=')
      ->execute()->fetchObject();
    return $data;
  }

  /**
   * Function to check existence of a menu in our table.
   */
  public function exists($plugin_id) {
    $data = $this->db->select('image_menu', 'im')
      ->fields('im')
      ->condition('plugin_id', $plugin_id, '=')
      ->execute()->fetchField();
    return (bool) $data;
  }

  /**
   * Function to add data.
   */
  public function add(&$arr, $plugin_id) { 
    $query = $this->db->insert('image_menu');
    $query->fields(
      [
        'plugin_id' => $plugin_id,
        'fid' => $arr['image'][0],
        'image_alt_text' => $arr['image_alt_text'],
        'image_title' => $arr['image_title'],
          
      ]
    )->execute();
  }

  /**
   * Function to update data.
   */
  public function update(&$arr, $plugin_id) {
    $query = $this->db->update('image_menu');
    $query->fields(
      [
        'plugin_id' => $plugin_id,
        'fid' => $arr['image'][0],
        'image_alt_text' => $arr['image_alt_text'],
        'image_title' => $arr['image_title'],
      ]
    );
    $query->condition('plugin_id', $plugin_id);
    $query->execute();
  }
   /**
   * Function to menuid.
   */
  public function getPluginid($form_id, $menu_id){
    if ( $form_id == 'menu_link_content_menu_link_content_form') {
        $id = $menu_id;
        $plugin_id = \Drupal::entityManager()->getStorage('menu_link_content')->load($id)->getPluginId();
    }elseif ($form_id == 'menu_link_edit') {
        $plugin_id = $menu_id;
    }
    return $plugin_id;
}
   /**
   * Function to ImageUrl.
   */
    public function getImageurl($fid,$alt,$title){
        $image_html = [];
        $data = $this->db->select('file_managed', 'fm')
        ->fields('fm',['uri'])
        ->condition('fid', $fid, '=')
        ->execute()->fetchObject();
    $image_url = file_create_url($data->uri);
    if($image_url != NULL){
            $image_html = "<span><img src='$image_url'alt= ".$alt." title = ".$title."></span>";
    }
    
    return $image_html;
    }
}
