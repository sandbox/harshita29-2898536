<?php

namespace Drupal\image_menu;

use Drupal\image_menu\StorageHelper;

/**
 * TwigExtension class returns menus attribute for each menu item and link.
 */
class TwigExtension extends \Twig_Extension {

  /**
   * In this function we define a twig extension name.
   */
  public function getName() {
    return 'image_menu';
  }

  /**
   * In this function we can declare the extension function.
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('image_menu', [$this, 'menusImage'],['is_safe' => ['html']]),
    ];
  }
  /**
   * Returns added attributes for list.
   */
  public function menusImage($plugin_id) { 
    $image_uri = '';  
    $instance = StorageHelper::instance();
    if ($instance->exists($plugin_id)) { 
        $data = $instance->getData($plugin_id);
        $image_html = ($data->fid == NULL) ? '' : $instance->getImageurl($data->fid,$data->image_alt_text,$data->image_title);
    }
    return $image_html;
  }
}
