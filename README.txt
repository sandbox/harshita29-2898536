INTRODUCTION
------------

By default, we can not add images in menu section.But, through this menu we can add an image against a menu.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module.

CONFIGURATION
-------------

 * Create a menu item.

 * Choose an Image against the menu.Image start to visible in menu section.

 